/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @brief   Fermentation chamber control program
 *
 * I/O map:
 * B6 - I2C SDA (LCD and temperature sensor)
 * B5 - Digital input (water level)
 * B4 - I2C SCL (LCD and temperature sensor)
 * B7 - LED
 * C2 - Analog input (temperature potentiometer)
 * C3 - Analog input (humidity potentiometer)
 * C1 - Digital output (heater)
 * C4 - Digital output (water pump)
 */

#include "config.h"
#include <xc.h>
#include "io.h"
#include "i2c.h"
#include "lcd.h"
#include "temp_sensor.h"

// Time, in tenths of a second, to allow the heating element to be on.
#define WARM_PERIOD 600

// Time, in tenths of a second, to allow the heating element to cool down.
#define COOL_PERIOD 100

// Number of milliseconds a reading should have the same value before it is
// accepted.
#define DEBOUNCE    10

enum {
    HEATER_OFF,
    HEATER_ON,
    HEATER_COOL
};

/**
 * Unsigned 8-bit value with debouncing.
 */
typedef struct
{
    uint8_t value;
    uint8_t new_value;
    uint8_t debounce;
} stable_uint8_t;

static uint8_t          seconds;
static uint8_t          minutes;
static uint8_t          hours;
static stable_uint8_t   cur_temp;
static stable_uint8_t   cur_humidity;
static stable_uint8_t   target_temp;
static stable_uint8_t   target_humidity;
static int8_t           update_display;
static uint16_t         heater_counter;
static int8_t           heater_state = HEATER_OFF;
static int8_t           low_water = 0;

/**
 * Update the value of a stable variable.
 * The variable is only updated once a new value has been observed repeatedly
 * for the debounce period.
 * @param   var     The variable to upadte
 * @param   value   The new value
 * @return  1 if a new value was set, 0 otherwise
 */
static int8_t
set_stable_uint8(stable_uint8_t *var, uint8_t value)
{
    if (var->new_value != value) {
        var->new_value = value;
        var->debounce = DEBOUNCE;
        return 0;
    }

    if (var->debounce > 0) {
        var->debounce--;
        return 0;
    }

    if (value == var->value) {
        return 0;
    }

    var->value = value;
    return 1;
}

/**
 * Configure the oscillator to 4MHz.
 * Must be called at the very beginning of the program.
 */
static void
oscillator_init(void)
{
    OSCEN = 0;
    OSCFRQ = 0x2; // 4MHz
    OSCTUNE = 0;

    // Wait for the high-frequency oscillator to be ready.
    for (;;) {
        if ((OSCSTAT & (1 << 6)) != 0) {
            break;
        }
    }
}

/**
 * Configure the I/O ports.
 */
static void
io_init(void)
{
    // Initialize all ports to digital output and set to low.
    // This will keep unused ports as output/low, per manual recommendation.
    LATA = 0x0;
    LATB = 0x0;
    LATC = 0x0;

    TRISA = 0xff;
    TRISB = 0xff;
    TRISC = 0xff;

    // Must set the output function for B6/B4 to SDA/SCL.
    RB6PPS = 0x8;
    RB4PPS = 0x7;

    // Configure SDA and SCL pins as digital inputs.
    TRISBbits.TRISB4 = 1;
    TRISBbits.TRISB6 = 1;
    ANSELBbits.ANSB4 = 0;
    ANSELBbits.ANSB6 = 0;

    // Setup Timer 1 for reading cycles in 16-bit words, using Fosc/4 as the
    // clock source. With a 4MHz Fosc this means 1MHz clock.
    T1CLK = 0x1;
    T1CON = 0x3;

    // Analog input on C2 for temperature knob.
    TRISCbits.TRISC2 = 1;
    ANSELCbits.ANSC2 = 1;

    // Analog input on C3 for humidity knob.
    TRISCbits.TRISC3 = 1;
    ANSELCbits.ANSC3 = 1;

    // Configure ADC.
    // Fosc/16 clock, VDD ref, right-justified.
    ADCON1 = 0xd0;

    // Start ADC on C2.
    ADCON0 = 0x49;

    // Configure C1 as digital output.
    TRISCbits.TRISC1 = 0;
    HEATER_OUT = 0;

    // Configure B5 as digital input.
    TRISBbits.TRISB5 = 1;
    ANSELBbits.ANSB5 = 0;

    // Configure C4 as digital output.
    TRISCbits.TRISC4 = 0;
    PUMP_OUT = 0;

    // Configure B7 as digital output.
    TRISBbits.TRISB7 = 0;
}

/**
 * Read timer 1.
 * Timer 1 is set to a frequency of 1MHz in setup_io() (assuming a 4MHz Fosc).
 * @return  The current time, in microseconds
 */
static uint16_t
get_time_us(void)
{
    uint8_t time_l = TMR1L;
    uint8_t time_h = TMR1H;
    return ((uint16_t)time_h << 8) + time_l;
}

/**
 * Keep track of time.
 * @param   delta_ms    Number of milliseconds since the last time the function
 *                      was called.
 */
static void
update_time(uint16_t delta_ms)
{
    static uint16_t ms;

    ms += delta_ms;
    if (ms < 1000) {
        return;
    }

    ms -= 1000;
    seconds++;
    if (seconds < 60) {
        return;
    }

    seconds = 0;
    minutes++;
    update_display = 1;
    if (minutes < 60) {
        return;
    }

    minutes = 0;
    hours++;
}

/**
 * Read the temperature sensor.
 */
static void
update_temp()
{
    if (set_stable_uint8(&cur_temp, (uint8_t)temp_sensor_read_temp())) {
        update_display = 1;
    }

    if (set_stable_uint8(&cur_humidity, (uint8_t)temp_sensor_read_humidity())) {
        update_display = 1;
    }
}

/**
 * Read the ADC to determine the target temperature or humidity.
 * Alternates between the two analog inputs, which share a single ADC unit.
 */
static void
get_target_temp_humidity(void)
{
    static int target_is_temp = 1;
    uint16_t result;

    // Analog readout.
    ADCON0bits.GO = 1;
    while (ADCON0bits.GO);

    result = ADRESH;
    result <<= 8;
    result |= ADRESL;

    if (target_is_temp) {
        // Set temperature target and switch ADC to C3.
        result = (result >> 5) + 24;
        if (set_stable_uint8(&target_temp, (uint8_t)result)) {
            update_display = 1;
        }

        ADCON0 = 0x4d;
        target_is_temp = 0;
    } else {
        // Set humidity target and switch ADC to C2.
        // If the value is small we keep the target as 0%, so that the pump
        // doesn't work at all.
        result = result >> 4;
        if (result < 4) {
            result = 0;
        } else {
            result += 30;
        }
        if (set_stable_uint8(&target_humidity, (uint8_t)result)) {
            update_display = 1;
        }
        ADCON0 = 0x49;
        target_is_temp = 1;
    }
}

/**
 * Turns the heating element on/off, based on the current and target
 * temperatures.
 */
static void
heater_control(void)
{
    if (heater_state == HEATER_COOL) {
        // Cooling period.
        if (heater_counter > 0) {
            heater_counter--;
            return;
        }
    }

    if (cur_temp.value >= target_temp.value) {
        // Current temperature at or above target.
        if (heater_state == HEATER_ON) {
            // Just reached the target temperature, turn off element and start a
            // cooling period.
            HEATER_OUT = 0;
            heater_state = HEATER_COOL;
            heater_counter = COOL_PERIOD;
            update_display = 1;
        } else if (heater_state == HEATER_COOL) {
            // Cooling period is over.
            heater_state = HEATER_OFF;
        } else {
            // Already off, nothing to do.
        }
    } else {
        // Current temperature is below target.
        if (heater_state == HEATER_ON) {
            // Element already on, check if needs a cooling period.
            if (heater_counter > 0) {
                heater_counter--;
            } else {
                HEATER_OUT = 0;
                heater_state = HEATER_COOL;
                heater_counter = COOL_PERIOD;
                update_display = 1;
            }
        } else {
            // Heater was off, turn it on.
            HEATER_OUT = 1;
            heater_state = HEATER_ON;
            heater_counter = WARM_PERIOD;
            update_display = 1;
        }
    }
}

/**
 * Turn the water pump on/off to control humidity.
 */
static void
pump_control(void)
{
    static uint16_t delay = 100;

    // First, check for changes to the low-water level detector.
    if (LOW_WATER_IN != low_water) {
        if (LOW_WATER_IN == 1) {
            // Low-water level detected. Stop the pump.
            PUMP_OUT = 0;
            low_water = 1;
        } else {
            // Water level restored, but wait 10 seconds before resuming
            // pump.
            delay = 100;
            low_water = 0;
        }

        update_display = 1;
    }

    if (cur_humidity.value >= target_humidity.value) {
        // Target humidity reached.
        if (PUMP_OUT == 1) {
            PUMP_OUT = 0;
            delay = 100;
            update_display = 1;
        }
        return;
    }

    if (low_water == 1) {
        return;
    }

    if (delay > 0) {
        delay--;
        return;
    }

    if (PUMP_OUT == 0) {
        // Turn pump on for 1 second.
        PUMP_OUT = 1;
        delay = 10;
        update_display = 1;
    } else {
        // Turn pump off for 30 seconds.
        PUMP_OUT = 0;
        delay = 300;
        update_display = 1;
    }
}

/**
 * Write to the LCD screen.
 */
static void
display(void)
{
    // Show temperature.
    lcd_select_line(1);
    lcd_write_string("T:");
    lcd_write_number(cur_temp.value, 2);
    lcd_write_string("/");
    lcd_write_number(target_temp.value, 2);
    lcd_write_string("C   ");

    // Show time.
    lcd_write_number(hours, 2);
    lcd_write_string(":");
    lcd_write_number(minutes, 2);

    // Show humidity.
    lcd_select_line(2);
    lcd_write_string("H:");
    lcd_write_number(cur_humidity.value, 2);
    lcd_write_string("/");
    lcd_write_number(target_humidity.value, 2);
    lcd_write_string("% ");

    // Water pump status.
    if (low_water == 1) {
        lcd_write_string("  LW ");
    } else if (PUMP_OUT == 1) {
        lcd_write_string("  WT ");
    } else {
        lcd_write_string("     ");
    }

    // Heater status.
    if (HEATER_OUT == 1) {
        lcd_write_string("HT");
    } else {
        lcd_write_string("  ");
    }

    update_display = 0;
}

void
main()
{
    // Initialization.
    oscillator_init();
    io_init();
    i2c_init();
    lcd_init();

    // Call twice to get initial values for temperature and humidity targets.
    get_target_temp_humidity();
    get_target_temp_humidity();

    int temp_sensor_avail = temp_sensor_init();

    uint16_t    last_us = 0;
    uint16_t    ms_count = 0;
    uint8_t     blink = 0;

    for (;;) {
        // Get current time.
        uint16_t us = get_time_us();
        if ((us - last_us) < 1000) {
            // Less than one millisecond, spin.
            continue;
        }

        uint16_t ms = (us - last_us) / 1000;
        ms_count += ms;
        last_us = us;

        if (ms_count >= 100) {
            // Run main control loop every 100ms.
            update_time(100);
            update_temp();
            get_target_temp_humidity();
            heater_control();
            pump_control();
            ms_count -= 100;

            blink++;
            if (blink == 5) {
                LED_OUT = 1 - LED_OUT;
                blink = 0;
            }
        } else {
            // Re-read in-flux stable variables.
            if ((cur_temp.debounce != 0)
                || (cur_humidity.debounce != 0)) {
                update_temp();
            }

            if ((target_temp.debounce != 0)
                || (target_humidity.debounce != 0)) {
                get_target_temp_humidity();
            }

        }

        if (update_display) {
            display();
        }
    }
}
