CC=	/opt/microchip/xc8/v2.40/bin/xc8-cc
CPU=	16F15244
CFLAGS=	-mcpu=$(CPU) -fno-short-double -fno-short-float -O2 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall -mdefault-config-bits   -std=c99 -gdwarf-3 -mstack=compiled:auto:auto -DXPRJ_default=default

NAME=	ferment
TARGET=	build/$(NAME).elf
MAP=	build/$(NAME).map
MEM=	build/memoryfile.xml
SRC=	$(wildcard *.c)
OBJS=	$(patsubst %.c,build/%.p1,$(SRC))

IPEDIR=	/opt/microchip/mplabx/v6.15/mplab_platform/mplab_ipe
IPICMD=	./ipecmd.sh -P16F15244 -TPPK4 -M -OL -F$(CURDIR)/build/$(NAME).hex

all: $(TARGET)

$(TARGET): build $(OBJS)
	$(CC) $(CFLAGS) -Wl,-Map=$(MAP) -Wl,--defsym=__MPLAB_BUILD=1 -Wl,--memorysummary,$(MEM) -o $@  $(OBJS)

build/%.p1: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

build:
	mkdir -p build

clean:
	rm -f build/*

flash: $(TARGET)
	cd $(IPEDIR) && $(IPICMD)
