/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "config.h"
#include <xc.h>
#include "i2c.h"
#include "io.h"

/**
 * Wait an idle condition.
 */
static void
i2c_master_wait(void)
{
    for (;;) {
        if ((SSP1STAT & 0x4) != 0) {
            // Transmission in progress.
            continue;
        }

        if ((SSP1CON2 & 0x1f) != 0) {
            // Not in idle state.
            continue;
        }

        break;
    }
}

static int
i2c_master_start()
{
    i2c_master_wait();

    PIR1bits.SSP1IF = 0;
    SSP1CON2 |= 0x1;
    for (int i = 0; i < 100; i++) {
        if (PIR1bits.SSP1IF != 0) {
            PIR1bits.SSP1IF = 0;
            return 1;
        }
    }

    return 0;
}

static void
i2c_master_stop()
{
    i2c_master_wait();

    PIR1bits.SSP1IF = 0;
    SSP1CON2 |= 0x4;
    while (PIR1bits.SSP1IF == 0);
    PIR1bits.SSP1IF = 0;
}

static void
i2c_master_write(uint8_t byte)
{
    i2c_master_wait();
    SSP1BUF = byte;
    while (PIR1bits.SSP1IF == 0);
    PIR1bits.SSP1IF = 0;
}

/**
 * LED pattern in case of an I2C error.
 * The pattern is one long, two short.
 */
static void
i2c_error(void)
{
    for (;;) {
        LED_OUT = 1;
        __delay_ms(500);
        LED_OUT = 0;
        __delay_ms(200);
        LED_OUT = 1;
        __delay_ms(200);
        LED_OUT = 0;
        __delay_ms(200);
        LED_OUT = 1;
        __delay_ms(200);
        LED_OUT = 0;
        __delay_ms(500);
    }
}

/**
 * Initialize I2C module 1.
 */
void
i2c_init(void)
{
    // Configure I2C for 100KHz (assuming a 4MHz oscillator).
    SSP1CON1bits.SSPM = 0x08;
    SSP1CON2 = 0;
    SSP1ADD = 0x9;
    SSP1STAT = 0;

    // Enable start/stop condition interrupts.
    SSP1CON3bits.SCIE = 1;
    SSP1CON3bits.PCIE = 1;

    // Enable I2C.
    SSP1CON1bits.SSPEN = 1;
}

void
i2c_write_data(uint8_t addr, uint8_t *data, uint8_t len)
{
    if (!i2c_master_start()) {
        i2c_error();
    }

    // Write slave address with the R/W bit cleared.
    i2c_master_write((uint8_t)((int)addr << 1));

    for (uint8_t i = 0; i < len; i++) {
        i2c_master_write(data[i]);
    }

    i2c_master_stop();
}

void
i2c_read_data(uint8_t addr, uint8_t *data, uint8_t len)
{
    i2c_master_start();

    // Write slave address with the R/W bit set.
    i2c_master_write((uint8_t)(((int)addr << 1) | 1));

    for (uint8_t i = 0; i < len; i++) {
        // Set RCEN.
        SSP1CON2 |= 0x8;

        // Wait for ACK.
        while (PIR1bits.SSP1IF == 0);
        PIR1bits.SSP1IF = 0;

        data[i] = SSP1BUF;
    }

    i2c_master_stop();
}
