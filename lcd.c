/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    lcd.c
 * @brief   Control two-line alphanumeric LCD via PCF8574 I2C module
 */

#include "config.h"
#include <xc.h>
#include "i2c.h"

#define LCD_I2C_ADDR            0x27

#define LCD_DATA_BYTE_ON        0x0D
#define LCD_DATA_BYTE_OFF       0x09
#define LCD_CMD_BYTE_ON         0x0C
#define LCD_CMD_BYTE_OFF        0x08
#define LCD_BACKLIGHTON_MASK    0x0F
#define LCD_BACKLIGHTOFF_MASK   0x07

#define LCD_CLEAR_CMD       0x01
#define LCD_HOME_CMD        0x02

#define LCD_ENTRY_CMD       0x04
#define  LCD_ENTRY_INC      0x02
#define  LCD_ENTRY_DEC      0x00
#define  LCD_ENTRY_SHIFT    0x01

#define LCD_DISPLAY_CMD     0x08
#define  LCD_DISPLAY_ON     0x04
#define  LCD_DISPLAY_OFF    0x00
#define  LCD_DISPLAY_CURSOR 0x02
#define  LCD_DISPLAY_BLINK  0x01

#define LCD_FUNCTION_CMD    0x20
#define  LCD_FUNCTION_8BIT  0x10
#define  LCD_FUNCTION_4BIT  0x00
#define  LCD_FUNCTION_2LINE 0x08
#define  LCD_FUNCTION_1LINE 0x00

#define LCD_LINE1_ADDR      0x80
#define LCD_LINE2_ADDR      0xC0
#define LCD_LINE3_ADDR      0x94
#define LCD_LINE4_ADDR      0xD4

static int backlight = LCD_BACKLIGHTON_MASK;
static uint8_t const line_addrs[] = {
    LCD_LINE1_ADDR,
    LCD_LINE2_ADDR,
    LCD_LINE3_ADDR,
    LCD_LINE4_ADDR
};

/**
 * Write an 8-bit command value to the LCD display.
 * @param   cmd     The value to write
 */
static void
lcd_write_cmd(uint8_t cmd)
{
    uint8_t bytes[] = {
        (cmd & 0xf0) | (LCD_CMD_BYTE_ON & backlight),
        (cmd & 0xf0) | (LCD_CMD_BYTE_OFF & backlight),
        ((cmd << 4) & 0xf0) | (LCD_CMD_BYTE_ON & backlight),
        ((cmd << 4) & 0xf0) | (LCD_CMD_BYTE_OFF & backlight)
    };

    i2c_write_data(LCD_I2C_ADDR, bytes, 4);
}

/**
 * Write an 8-bit data value to the LCD display.
 * Services all other lcd_write_*() functions.
 * @param   data    The value to write
 */
static void
lcd_write_data(uint8_t data)
{
    uint8_t bytes[] = {
        (data & 0xf0) | (LCD_DATA_BYTE_ON & backlight),
        (data & 0xf0) | (LCD_DATA_BYTE_OFF & backlight),
        ((data << 4) & 0xf0) | (LCD_DATA_BYTE_ON & backlight),
        ((data << 4) & 0xf0) | (LCD_DATA_BYTE_OFF & backlight)
    };

    i2c_write_data(LCD_I2C_ADDR, bytes, 4);
}

/**
 * Select the line to write to.
 * @param   line    1-based line number
 */
void
lcd_select_line(uint8_t line)
{
    lcd_write_cmd(line_addrs[line - 1]);
}

/**
 * Write a NUL-terminated string to the display.
 * @param   num     The number to write
 * @param   digits  The number of digits to display
 */
void
lcd_write_string(char const *str)
{
    for (uint8_t const *data = (uint8_t const *)str; *data != '\0'; data++) {
        lcd_write_data(*data);
    }
}

/**
 * Write a decimal value to the LCD.
 * @param   num     The number to write
 * @param   digits  The number of digits to display
 */
void
lcd_write_number(uint16_t num, uint8_t digits)
{
    char    str[8];
    char   *p = &str[digits - 1];
    uint8_t i;
    for (i = 0; i < digits; i++) {
        *p = '0' + num % 10;
        num = num / 10;
        p--;
    }

    for (i = 0; i < digits; i++) {
        lcd_write_data(str[i]);
    }
}

/**
 * Write a hexadecimal value to the LCD.
 * Used for debugging.
 * @param   num     The number to write
 * @param   digits  The number of digits to display
 */
void
lcd_write_hex(uint16_t num, uint8_t digits)
{
    char    str[8];
    char   *p = &str[digits - 1];
    uint8_t i;
    for (i = 0; i < digits; i++) {
        uint8_t ch = num & 0xf;
        if (ch < 10) {
            *p = '0' + ch;
        } else {
            *p = 'A' + ch - 10;
        }
        num >>= 4;
        p--;
    }

    for (i = 0; i < digits; i++) {
        lcd_write_data(str[i]);
    }
}

/**
 * Initialize the LCD module.
 */
void
lcd_init(void)
{
    __delay_ms(15);
    lcd_write_cmd(LCD_HOME_CMD);
    __delay_ms(5);
    lcd_write_cmd(LCD_HOME_CMD);
    __delay_ms(5);
    lcd_write_cmd(LCD_HOME_CMD);
    __delay_ms(5);

    lcd_write_cmd(LCD_FUNCTION_CMD | LCD_FUNCTION_4BIT | LCD_FUNCTION_2LINE);
    lcd_write_cmd(LCD_DISPLAY_CMD | LCD_DISPLAY_ON);
    lcd_write_cmd(LCD_ENTRY_CMD | LCD_ENTRY_INC);
    lcd_write_cmd(LCD_CLEAR_CMD);
    __delay_ms(5);
}
