Proofing/Fermentation Chamber
=============================

MCU-based circuit for controlling temperature and humidity in a sourdough
proofing chamber.

Controller
----------

Microchip PIC16F15244 (should be easy to substitute for other PIC16/PIC18 MCUs)

Inputs
------

* BME280 Temperature/humidity/pressure sensor
* 2 10Kohm potentiometers for dials
* Reed switch for water level
* Push button for forcing the valve open

Outputs
-------

* 100W, 12V heating element with fan
* 12V solenoid micro-valve
* 16x2 alphanumeric LCD with PCF8574 I2C expander module
* LED
