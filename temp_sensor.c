/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    temp_sensor.c
 * @brief   Read and interpret BME280 temperature and humidity values
 */

#include "config.h"
#include <xc.h>
#include "i2c.h"
#include "lcd.h"

#define SENSOR_I2C_ADDR         0x76

#define BME280_CHIPID           0xd0
#define BME280_STATUS           0xf3
#define BME280_CTRL_HUM         0xf2
#define BME280_CTRL_MEAS        0xf4

#define BME280_DIG_T1_1         0x88
#define BME280_DIG_T1_2         0x89
#define BME280_DIG_T2_1         0x8a
#define BME280_DIG_T2_2         0x8b
#define BME280_DIG_T3_1         0x8c
#define BME280_DIG_T3_2         0x8d

#define BME280_DIG_H1           0xa1
#define BME280_DIG_H2_1         0xe1
#define BME280_DIG_H2_2         0xe2
#define BME280_DIG_H3           0xe3
#define BME280_DIG_H4           0xe4
#define BME280_DIG_H4_5         0xe5
#define BME280_DIG_H5           0xe6
#define BME280_DIG_H6           0xe7

#define BME280_TEMP_MSB         0xfa
#define BME280_TEMP_LSB         0xfb
#define BME280_TEMP_XLSB        0xfc
#define BME280_HUM_MSB          0xfd
#define BME280_HUM_LSB          0xfe

enum {
    TRIM_PARAM_T1_1,
    TRIM_PARAM_T1_2,
    TRIM_PARAM_T2_1,
    TRIM_PARAM_T2_2,
    TRIM_PARAM_T3_1,
    TRIM_PARAM_T3_2,
    TRIM_PARAM_H1,
    TRIM_PARAM_H2_1,
    TRIM_PARAM_H2_2,
    TRIM_PARAM_H3,
    TRIM_PARAM_H4,
    TRIM_PARAM_H45,
    TRIM_PARAM_H5,
    TRIM_PARAM_H6,
    TRIM_PARAM_NUM
};

static uint8_t  trimming_params[TRIM_PARAM_NUM];
static uint32_t t_fine;

/**
 * Read all temperature and humidity coefficient values from NVRAM.
 */
static void
read_trimming_params(void)
{
    uint8_t     byte;

    // Read temperature parameters.
    uint8_t addr = BME280_DIG_T1_1;
    for (int i = TRIM_PARAM_T1_1; i <= TRIM_PARAM_T3_2; i++) {
        i2c_write_data(SENSOR_I2C_ADDR, &addr, 1);
        i2c_read_data(SENSOR_I2C_ADDR, &trimming_params[i], 1);
        addr++;
    }

    // Read humidity parameters.
    addr = BME280_DIG_H1;
    i2c_write_data(SENSOR_I2C_ADDR, &addr, 1);
    i2c_read_data(SENSOR_I2C_ADDR, &trimming_params[TRIM_PARAM_H1], 1);

    addr = BME280_DIG_H2_1;
    for (int i = TRIM_PARAM_H2_1; i <= TRIM_PARAM_H6; i++) {
        i2c_write_data(SENSOR_I2C_ADDR, &addr, 1);
        i2c_read_data(SENSOR_I2C_ADDR, &trimming_params[i], 1);
        addr++;
    }
}

static inline uint16_t
dig_T1(void)
{
    return (((uint16_t)trimming_params[TRIM_PARAM_T1_2] << 8) |
            trimming_params[TRIM_PARAM_T1_1]);
}

static inline uint16_t
dig_T2(void)
{
    return (((uint16_t)trimming_params[TRIM_PARAM_T2_2] << 8) |
            trimming_params[TRIM_PARAM_T2_1]);
}

static inline uint16_t
dig_T3(void)
{
    return (((uint16_t)trimming_params[TRIM_PARAM_T3_2] << 8) |
            trimming_params[TRIM_PARAM_T3_1]);
}

static inline uint8_t
dig_H1(void)
{
    return trimming_params[TRIM_PARAM_H1];
}

static inline uint16_t
dig_H2(void)
{
    return (((uint16_t)trimming_params[TRIM_PARAM_H2_2] << 8) |
            trimming_params[TRIM_PARAM_H2_1]);
}

static inline uint8_t
dig_H3(void)
{
    return trimming_params[TRIM_PARAM_H3];
}

static inline uint16_t
dig_H4(void)
{
    return (((uint16_t)trimming_params[TRIM_PARAM_H4] << 4) |
            (trimming_params[TRIM_PARAM_H45] & 0xf));
}

static inline uint16_t
dig_H5(void)
{
    return (((uint16_t)trimming_params[TRIM_PARAM_H5] << 4) |
            (trimming_params[TRIM_PARAM_H45] >> 4));
}

static inline uint8_t
dig_H6(void)
{
    return trimming_params[TRIM_PARAM_H6];
}

/**
 * Initialize the sensor.
 */
int
temp_sensor_init(void)
{
    uint8_t bytes[2];

    // Get the ID value. Should read 0x60.
    bytes[0] = BME280_CHIPID;
    i2c_write_data(SENSOR_I2C_ADDR, &bytes[0], 1);
    i2c_read_data(SENSOR_I2C_ADDR, &bytes[1], 1);

    if (bytes[1] != 0x60) {
        return 0;
    }

    // Wait for update to finish.
    bytes[0] = BME280_STATUS;
    for (int i = 0; i < 1000; i++) {
        i2c_write_data(SENSOR_I2C_ADDR, &bytes[0], 1);
        i2c_read_data(SENSOR_I2C_ADDR, &bytes[1], 1);
        if ((bytes[1] & 1) == 0) {
            break;
        }
    }

    // Set x1 humidity sampling.
    bytes[0] = BME280_CTRL_HUM;
    bytes[1] = 0x1;
    i2c_write_data(SENSOR_I2C_ADDR, &bytes[0], 2);

    // Set normal mode with x1 temperature sampling.
    bytes[0] = BME280_CTRL_MEAS;
    bytes[1] = 0x23;
    i2c_write_data(SENSOR_I2C_ADDR, &bytes[0], 2);

    // Get coefficient values.
    read_trimming_params();

    return 1;
}

/**
 * Calculate the temperature value.
 * @param   measure The value read from the sensor
 * @return  The temperature, in 1/100 degrees Celsius
 */
static inline uint16_t
calc_temp(uint32_t measure)
{
    uint32_t t1 = dig_T1();
    uint32_t t2 = dig_T2();
    uint32_t t3 = dig_T3();

    measure >>= 3;
    uint32_t var1 = (((measure - (t1 << 1))) * t2) >> 11;

    measure >>= 1;
    uint32_t var2 = ((((measure - t1) * (measure - t1)) >> 12) * t3) >> 14;

    t_fine = var1 + var2;
    return (uint16_t)((t_fine * 5 + 128) >> 8);
}

/**
 * Calculate a humidity value in percentage.
 * The original formula produces the percentage in the most significant 22 bits
 * and the fractional value in the lower 10 bits. This function discards the
 * lower 10 bits.
 * @param   measure The value read from the sensor
 * @return  The humidity, in percentage
 */
static inline uint16_t
calc_humidity(uint32_t measure)
{
    uint32_t h1 = dig_H1();
    uint32_t h2 = dig_H2();
    uint32_t h3 = dig_H3();
    uint32_t h4 = dig_H4();
    uint32_t h5 = dig_H5();
    uint32_t h6 = dig_H6();

    uint32_t var1 = t_fine - 76800;
    uint32_t var2 = (((measure << 14) - (h4 << 20) - (h5 * var1)) + 16384) >> 15;
    uint32_t var3 = (var2 *
                     (((((((var1 * h6) >> 10) *
                          (((var1 * h3) >> 11) + 32768)) >> 10) + 2097152) * h2
                       + 8192) >> 14));
    uint32_t var4 = (var3 - (((((var3 >> 15) * (var3 >> 15)) >> 7) * h1) >> 4));
    if ((int32_t)var4 < 0) {
        var4 = 0;
    } else if (var4 > 419430400) {
        var4 = 419430400;
    }

    return (uint16_t)(var4 >> 22);
}

/**
 * @return  Sensor temperature value
 */
uint16_t
temp_sensor_read_temp()
{
    uint32_t    measure;
    uint8_t     byte;

    // Read temperature registers.
    byte = BME280_TEMP_MSB;
    i2c_write_data(SENSOR_I2C_ADDR, &byte, 1);
    i2c_read_data(SENSOR_I2C_ADDR, &byte, 1);
    measure = byte;
    measure <<= 8;

    byte = BME280_TEMP_LSB;
    i2c_write_data(SENSOR_I2C_ADDR, &byte, 1);
    i2c_read_data(SENSOR_I2C_ADDR, &byte, 1);
    measure |= byte;
    measure <<= 4;

    byte = BME280_TEMP_XLSB;
    i2c_write_data(SENSOR_I2C_ADDR, &byte, 1);
    i2c_read_data(SENSOR_I2C_ADDR, &byte, 1);
    measure |= (byte & 0xf);

    // Calculate temperature, using the formula from the datasheet.
    return calc_temp(measure) / 100;
}

/**
 * @return  Sensor humidity value
 */
uint16_t
temp_sensor_read_humidity()
{
    uint32_t    measure;
    uint8_t     byte;

    // Read humidity registers.
    byte = BME280_HUM_MSB;
    i2c_write_data(SENSOR_I2C_ADDR, &byte, 1);
    i2c_read_data(SENSOR_I2C_ADDR, &byte, 1);
    measure = byte;
    measure <<= 8;

    byte = BME280_HUM_LSB;
    i2c_write_data(SENSOR_I2C_ADDR, &byte, 1);
    i2c_read_data(SENSOR_I2C_ADDR, &byte, 1);
    measure |= byte;

    // Calculate humidity, using the formula from the datasheet.
    return calc_humidity(measure);
}
