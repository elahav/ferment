#ifndef IO_H
#define IO_H

// I/O aliases
#define HEATER_OUT      LATCbits.LATC1
#define PUMP_OUT        LATCbits.LATC4
#define LOW_WATER_IN    PORTBbits.RB5
#define LED_OUT         LATBbits.LATB7

#endif
